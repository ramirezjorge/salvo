package com.codeoftheweb.salvo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class SalvoController {

    @Autowired
    private GameRepository repoGames;

//    @RequestMapping("/api/games")
//    public List<Long> getIdGames() {
//
//        return repoGames.findAll().stream().map(game -> game.getId()).collect(Collectors.toList());
//        }

/** findAll() busca todos los games
 * stream() nos permite usar los demas
 * .map() recive uan condicion lambda la evalua y devuelve las treu
 * collect(Collectors.toList) para colocar los resultado en una lista, crea una nueva lista para los resultados
 */

@RequestMapping("/api/games")
public List<Map<String,Object>> getIdGames(){
    return repoGames.findAll().stream().map(game -> game.makeGameDTO(game)).collect(Collectors.toList());
}


}
